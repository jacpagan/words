#!/usr/bin/python
import random 
from word_parser import create_dict
from word_parser import find_word

adj = "adjectives.txt"
noun = "nouns.txt"
verb = "verbs.txt"

def combo(f1, f2): 
    f1 = open(f1)
    f2 = open(f2)
    r1, r2 = [], []
    for t1 in f1: 
        r1.append(t1)
    f1.close()
    for t2 in f2: 
        r2.append(t2)
    f2.close()
    return list((random.choice(r1).strip('\n'), random.choice(r2).strip('\n')))

def combo1(f1): 
    f1 = open(f1)
    r = []
    for t1 in f1: 
	    r.append(t1)
    f1.close()
    return list((random.choice(r).strip('\n'), 
    random.choice(r).strip('\n')))


for i in xrange(5): 
    n, v = combo(noun, verb)
    print "Word combination #{}".format(i+1) 
    print "NOUN: %s"% n
    print find_word(n)
    print "-----"*20
    print "VERB: %s"% v  
    print find_word(v)
    print "-----"*20
    print "-----"*20
    