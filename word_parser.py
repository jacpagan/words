#!/usr/bin/python
import argparse
import re
import collections
#import word_combinator
from collections import deque



def create_dict(): 
	result = {}
	dictionary_file = open('en.txt', "r")
	valid = re.compile(r'^(?![0-9])[A-Z]*[A-Z0-9 \-]+?$')
	for line in dictionary_file:
		if re.match(valid, line):
			k = line.rstrip() 
			if result.get(k) == None: 
				result[k]=[]
		else: 
			result[k].append(line.replace('\n', ''))
	return result


def show_words(d): 
	for k, v in sorted(d.items()): 
		print (k, '=', v)


def find_word(w): 
    d = create_dict()
    df = ""
    for i in d[w.upper().rstrip()]: 
        df += i 
    return df
    
def find_words(d):
	word = raw_input("Enter a word: ").lower()
	return d[word.upper().rstrip()]




#print show_words(d)
#print d
#print find_words(d)
