import random 
import pprint
import sys
"""
The program chooses 2 words at random from a list of: 
    - adjectives and nouns 
    - nouns and verbs  
    - verb and a noun 
    - noun and adjective 
    - noun and noun
"""
"""USAGE:  
    adj, noun = adj_noun() 

"""
class Combo(object):
    adj_f = open("adjectives.txt")
    noun_f = open("nouns.txt")    
    vrb_f = open("verbs.txt")
    
    def __init__(self):
        self.pair = self.get_all()
    def __init__(self, op): 
        self.pair = self.get(op)
    
        
    def get(self, op):
        list1 = [] 
        list2 = []
        if op == "nv": 
            for n in self.noun_f: 
                list1.append(n)
            for v in self.vrb_f: 
                list2.append(n)
        return list((random.choice(list1).strip('\n'), random.choice(list2).strip('\n')))
        if op == "vn": 
            for v in self.vrb_f: 
                list1.append(n)
            for n in self.noun_f: 
                list2.append(n)
        return list((random.choice(list1).strip('\n'), random.choice(list2).strip('\n')))
        if op == "na": 
            for n in self.noun_f: 
                list1.append(n)
            for a in self.adj_f: 
                list2.append(a)
        return list((random.choice(list1).strip('\n'), random.choice(list2).strip('\n')))
        if op == "nn": 
            for n in self.noun_f: 
                list1.append(n)
            for n in self.noun_f: 
                list2.append(n)
        return list((random.choice(list1).strip('\n'), random.choice(list2).strip('\n')))
        if op not in ["an", "nv", "vn", "na", "nn"]: 
            return TypeError
        self.adj_f.close()
        self.noun_f.close() 


x = Combo("an")    
print x.pair
def adj_verb(): 
    for a in self.adj_f: 
        list1.append(a)
    for n in self.noun_f: 
        list2.append(n)    
    return list((random.choice(list1).strip('\n'), random.choice(list2).strip('\n')))
def noun_verb(): 
    noun_f = open("nouns.txt")    
    vrb_f = open("verbs.txt")
    nouns = []
    verbs = []
    for n in noun_f: 
	    nouns.append(n)
    
    for v in vrb_f: 
        verbs.append(v)
    
    noun_f.close() 
    vrb_f.close()
    print 'Amongst ', len(nouns), ' nouns and ', len(verbs), 'verbs you chose:\n' 
    print 'noun: ', random.choice(nouns), 'verb: ', random.choice(verbs)
    
def verb_noun(): 
    vrb_f = open("verbs.txt")
    noun_f = open("nouns.txt")    
    verbs = []
    nouns = []
    for v in vrb_f: 
        verbs.append(v)
    for n in noun_f: 
	    nouns.append(n)
    vrb_f.close()
    noun_f.close() 
    print 'Amongst ', len(verbs), ' verbs and ', len(nouns), 'nouns you chose:\n' 
    print 'verb: ', random.choice(verbs), 'noun: ', random.choice(nouns)

def noun_adj():
    noun_f = open("nouns.txt")    
    adj_f = open("adjectives.txt")
    nouns = []
    adjectives = [] 
    for n in noun_f: 
	    nouns.append(n)
    for a in adj_f: 
	    adjectives.append(a)
    noun_f.close()
    adj_f.close()
    print 'Amongst ', len(nouns), ' nouns and ', len(adjectives), 'adjectives you chose:\n'
    print 'noun: ', random.choice(nouns), 'adjective: ', random.choice(adjectives)

def noun_noun(): 
    noun_f = open("nouns.txt")    
    nouns = []

    for n in noun_f: 
	    nouns.append(n)

    noun_f.close()
    print 'Amongst ', len(nouns), ' nouns you chose:\n'
    print 'noun: ', random.choice(nouns), 'noun: ', random.choice(nouns)

"""Append text to adjectives.txt"""
def add_adj(): 
    text = str(raw_input("Enter an adjective: ")) 
    if text not in open("adjectives.txt").read():
        with open("adj.txt", "a") as adj_f:
            adj_f.write(text) 
    else: 
        print text, 'is in the file so it won\'t be added'

"""PRINT FUNTIONS """
def list_adj(): 
    adj_f = open("adjectives.txt")    
    adjectives = [] 
    for a in adj_f: 
        adjectives.append(a)
    adj_f.close() 
    print 'There are ', len(adjectives), '  adjectives' 
    pp = pprint.PrettyPrinter(indent=2)
    pp.pprint(adjectives)

def list_nouns(): 
    noun_f = open("nouns.txt")    
    nouns = [] 
    for n in noun_f: 
        nouns.append(n)
    noun_f.close() 
    print 'There are ', len(nouns), '  nouns' 
    pp = pprint.PrettyPrinter(indent=2)
    pp.pprint(nouns)

def list_vrb(): 
    vrb_f = open("verbs.txt")    
    verbs = [] 
    for v in vrb_f: 
        verbs.append(v)
    vrb_f.close() 
    print 'There are ', len(verbs), '  verbs' 
    pp = pprint.PrettyPrinter(indent=2)
    pp.pprint(verbs)
    
"""DRIVER"""
def go(): 
    adj_count = 0 
    noun_count = 0
    upadj_count = 0 
    upnoun_count = 0
    adj, noun = adj_noun()
       
    print 'Write a lyric or a paragraph with the adjective: %s and the noun: %s '% (adj, noun)
    input_str = sys.stdin.read().split()
    print "************************************************************"
    print input_str
    print "************************************************************"

    for i in input_str: 
        if i == adj: 
            adj_count += 1 
        elif i == noun: 
            noun_count += 1

    for i in input_str: 
        if i == adj.upcase(): 
            upadj_count += 1 
        elif i == noun.upcase(): 
            upnoun_count += 1
                   
    print "You used the adjective ", adj, ', ', adj_count, ' times.'
    print "You used the noun ",noun, ', ', noun_count, ' times.'   
    print "You used the UPCASE adjective ", adj, ', ', upadj_count, ' times.'
    print "You used the UPCASE noun ",noun, ', ', upnoun_count, ' times.' 
    print "************************************************************"
"""
go()
noun_verb()
verb_noun() 
noun_adj() 
noun_noun()
#list_adj()
#list_nouns()
#list_vrb()
"""
