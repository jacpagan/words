# README #

### What is this repository for? ###
Programmers who want to use English language words and definitions in their applications. 

* Quick summary
Word_parser reads from the Project Gutenberg dictionary and creates a python dictionary in O(n) time-complexity and for easy O(1) time-complexity lookup. 

### How do I get set up? ###

cd Words/ 

python word_parser.py 

* These files must be inside the same directory level as word_parser.py
	- en.txt

### Who do I talk to? ###

* Repo owner or admin
Jose Pagan